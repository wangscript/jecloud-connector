/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.service;

import com.je.common.base.message.vo.*;
import com.je.connector.base.protocol.Packet;
import java.util.List;

public interface PushService {

    void sendMsg(String userId, PushMessage message);

    void sendMsg(List<String> userIdList,PushMessage message);

    /**
     * 发送未读消息
     * @param userId
     * @param message
     */
    void sendNoReadMsg(String userId, PushNoReadMessage message);

    /**
     * 发送未读消息
     * @param userIdList
     * @param message
     */
    void sendNoReadMsg(List<String> userIdList,PushNoReadMessage message);

    /**
     * 发送系统消息
     * @param userId
     * @param message
     */
    void sendSystemMsg(String userId, PushSystemMessage message);

    /**
     * 发送系统消息
     * @param userIdList
     * @param message
     */
    void sendSystemMsg(List<String> userIdList, PushSystemMessage message);

    /**
     * 发送其他类型消息
     * @param userIdList
     * @param message
     */
    void sendOtherSystemMsg(List<String> userIdList, PushMessage message);

    /**
     * 发送脚本消息
     */
    void sendScriptMsg(String userId, PushScriptMessage message);

    /**
     * 发送脚本消息
     */
    void sendScriptMsg(List<String> userIdList, PushScriptMessage message);

    /**
     * 发送脚本消息
     */
    void sendScriptOpenFuncGridMsg(String userId,String funcCode,String busType, String content);

    /**
     * 发送脚本消息
     */
    void sendScriptOpenFuncGridMsg(List<String> userIdList, String funcCode,String busType, String content);

    /**
     * 发送脚本消息
     */
    void sendScriptOpenFuncFormMsg(String userId, String funcCode,String busType, String content,String beanId);

    /**
     * 发送脚本消息
     */
    void sendScriptOpenFuncFormMsg(List<String> userIdList, String funcCode,String busType, String content,String beanId);

    /**
     * 发送脚本消息
     */
    void sendNoticeMsg(String userId, PushNoticeMessage message);

    /**
     * 发送脚本消息
     */
    void sendNoticeMsg(List<String> userIdList, PushNoticeMessage message);

    /**
     * 发送脚本消息
     */
    void sendNoticeOpenFuncGridMsg(String userId, String funcCode,String busType, Notice content);

    /**
     * 发送脚本消息
     */
    void sendNoticeOpenFuncGridMsg(List<String> userIdList, String funcCode,String busType, Notice content);

    /**
     * 发送脚本消息
     */
    void sendNoticeOpenFuncFormMsg(String userId, String funcCode,String busType, Notice content,String beanId);

    /**
     * 发送脚本消息
     */
    void sendNoticeOpenFuncFormMsg(List<String> userIdList, String funcCode,String busType, Notice content,String beanId);

    /**
     * 所有的公共发送方法
     * @param userIdList 用户id集合
     * @param packet      包信息
     */
    void sendToConnection(List<String> userIdList, Packet packet);

}
