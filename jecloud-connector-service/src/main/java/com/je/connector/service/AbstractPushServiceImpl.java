/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.service;

import com.je.connector.base.connection.Connection;
import com.je.connector.base.protocol.Packet;
import com.je.connector.server.InstantServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public abstract class AbstractPushServiceImpl implements PushService {

    @Autowired
    protected InstantServer instantServer;

    public List<Connection> getConnections(List<String> userIdList) {
        log.info("listAddUserId=,{}", userIdList.toString());
        List<Connection> connectionList = instantServer.getConnectionRegistry().getLocalConnections();
        List<Connection> userConnections = new ArrayList<>();
        for (Connection eachConnection : connectionList) {
            log.info("eachConnection=,{}", eachConnection.getUserId());
            if (userIdList.contains(eachConnection.getUserId())) {
                log.info("listAddUserId=,{}", eachConnection.getUserId());
                userConnections.add(eachConnection);
            }
        }
        return userConnections;
    }

    @Override
    public void sendToConnection(List<String> userIdList, Packet packet) {
        List<Connection> connections = getConnections(userIdList);
        if (null == connections || connections.isEmpty()) {
            return;
        }
        for (Connection connection : connections) {
            connection.channelSend(packet);
        }
    }

}
