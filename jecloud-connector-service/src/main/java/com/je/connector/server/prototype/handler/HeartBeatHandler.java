/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.server.prototype.handler;

import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Charsets;
import com.je.connector.base.connection.Connection;
import com.je.connector.base.message.MessageHandler;
import com.je.connector.base.protocol.Command;
import com.je.connector.base.protocol.Packet;
import com.je.connector.server.InstantServer;
import lombok.extern.slf4j.Slf4j;

/**
 * 心跳消息处理器
 *
 * @ProjectName: instant-message
 * @Package: com.connector.server.handler
 * @ClassName: HeartBeatHandler
 * @Description: 定义心跳消息处理器
 * @Author: LIULJ
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Slf4j
public class HeartBeatHandler implements MessageHandler {

    private InstantServer instantServer;

    public HeartBeatHandler(InstantServer instantServer) {
        this.instantServer = instantServer;
    }

    @Override
    public void handle(Packet packet, Connection connection) {
        //ping -> pong
        Packet heartbeat = Packet.HEARTBEAT_RESP_PAFKET;
        JSONObject heartbeatMessage = new JSONObject();
        heartbeatMessage.put("type", Command.OK.cmd);
        byte[] body = heartbeatMessage.toJSONString().getBytes(Charsets.UTF_8);
        heartbeat.setBody(body);
        connection.channelSend(heartbeat);
        log.info("ping -> pong, {}", connection);
    }
}
