/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.server.timer;

import com.je.connector.base.connection.Connection;
import com.je.connector.server.InstantServer;
import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;
import io.netty.util.Timer;
import io.netty.util.TimerTask;
import lombok.extern.slf4j.Slf4j;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
public class HandshakeCheckTimer {

    private final InstantServer instantServer;

    private Timer timer;

    public HandshakeCheckTimer(InstantServer instantServer) {
        this.instantServer = instantServer;
        timer = new HashedWheelTimer(Executors.defaultThreadFactory(), 2, TimeUnit.MINUTES, 1);
    }

    /**
     * 启动心跳检测
     */
    public void start(){
        List<Connection> connections = instantServer.getConnectionRegistry().getConnections();
        TimerTask task1 = new TimerTask() {
            @Override
            public void run(Timeout timeout) throws Exception {
                checkHandshake(connections);
                timer.newTimeout(this, 1, TimeUnit.SECONDS);//结束时候再次注册
            }
        };
        timer.newTimeout(task1, 1, TimeUnit.SECONDS);
    }

    /**
     * 停止心跳检测
     */
    public void stop(){
        timer.stop();
    }

    public void checkHandshake(List<Connection> connections) {

    }

}
