/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.server.prototype.handler;

import com.google.common.base.Strings;
import com.je.connector.base.message.model.*;
import com.je.connector.server.InstantServer;
import com.je.connector.base.connection.Connection;
import com.je.connector.base.connection.SessionContext;
import com.je.connector.base.exception.handler.BaseMessageHandler;
import com.je.connector.base.protocol.Packet;
import com.je.connector.base.utils.ErrorCode;
import lombok.extern.slf4j.Slf4j;

/**
 * 证书检测
 */
@Slf4j
public class HandShakeCheckHandler extends BaseMessageHandler<HandshakeCheckMessage> {

    private InstantServer instantServer;

    public HandShakeCheckHandler(InstantServer instantServer) {
        this.instantServer = instantServer;
    }

    @Override
    public HandshakeCheckMessage decode(Packet packet, Connection connection) {
        return new HandshakeCheckMessage(packet, connection);
    }

    @Override
    public void handle(HandshakeCheckMessage message) {
        //1.校验客户端消息字段
        String msg = message.getMsg();
        if (Strings.isNullOrEmpty(msg)) {
            HandshakeMessage.buildErrorResponse(message.getConnection()).setErrorCode(ErrorCode.INVALID_DEVICE).buildResult("Param invalid", "发送消息不能为空！").close();
            log.error("handshake check error, message={}, conn={}", message, message.getConnection());
            return;
        }

        //2.重复握手判断
        SessionContext context = message.getConnection().getSessionContext();
        String appId = context.getAppid();
        //2.校验客户端appId字段
        if (Strings.isNullOrEmpty(appId)) {
            HandshakeMessage.buildErrorResponse(message.getConnection()).setErrorCode(ErrorCode.INVALID_DEVICE).buildResult("Param invalid", "发送消息不能为空！").close();
            log.error("handshake check error, message={}, conn={}", message, message.getConnection());
            return;
        }

        try {
            //认证通过
            context.setTime2(System.currentTimeMillis());
        } catch (Throwable e) {
            HandshakeMessage.buildErrorResponse(message.getConnection()).setErrorCode(ErrorCode.UNKNOWN).buildResult(e.getMessage(), "证书检测失败！").close();
            log.error("chat failure!", e);
        }
    }

}
