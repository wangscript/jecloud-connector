/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.util;

import com.je.connector.base.connection.SessionContext;
import com.je.connector.base.message.model.HandshakeMessage;

import java.util.UUID;

public class SessionContextUtil {

    public static SessionContext buildSessionContext(HandshakeMessage message) {
        SessionContext sessionContext = new SessionContext();
        sessionContext.setClientVersion(message.getClientVersion());
        sessionContext.setDeviceId(message.getDeviceId());
        sessionContext.setOsName(message.getOsName());
        sessionContext.setOsVersion(message.getOsVersion());
        sessionContext.setUserId(message.getUserId());
        sessionContext.setMobile(message.getMobile());
        sessionContext.setTime1(System.currentTimeMillis());
        sessionContext.setUuid(UUID.randomUUID().toString());
        sessionContext.setCid(message.getCid());
        sessionContext.setAppid(message.getAppid());
        return sessionContext;
    }

}
