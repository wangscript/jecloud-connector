/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.rpc;

import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ConnectionRegistryRpcServiceImpl implements ConnectionRegistryRpcService {

    @RpcReference(microserviceName = "connector",schemaId = "connectionRegistryRpcService")
    private ConnectionRegistryRpcService connectionRegistryRpcService;

    @Override
    public boolean exist(String connectionId) {
        return connectionRegistryRpcService.exist(connectionId);
    }

    @Override
    public void putApp(String appId) {
        connectionRegistryRpcService.putApp(appId);
    }

    @Override
    public void putAppAndConnectId(String appId, String connectorId) {
        connectionRegistryRpcService.putAppAndConnectId(appId,connectorId);
    }

    @Override
    public void putUserAndConnectorId(String userId, String deviceId, String connectorId) {
        connectionRegistryRpcService.putUserAndConnectorId(userId,deviceId,connectorId);
    }

    @Override
    public String getAppIdByConnector(String connectorId) {
        return connectionRegistryRpcService.getAppIdByConnector(connectorId);
    }

    @Override
    public String getAppIdByUser(String userId, String deviceId) {
        return connectionRegistryRpcService.getAppIdByUser(userId,deviceId);
    }

    @Override
    public List<String> getAllApps() {
        return connectionRegistryRpcService.getAllApps();
    }

    @Override
    public List<String> getAppConnectors(String appId) {
        return connectionRegistryRpcService.getAppConnectors(appId);
    }

    @Override
    public long size() {
        return connectionRegistryRpcService.size();
    }

}
