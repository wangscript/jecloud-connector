/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.connection;

import com.je.connector.base.connection.channel.SocketChannel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;

/**
 * 定义连接
 *
 * @ProjectName: instant-message
 * @Package: com.connector.base.connection
 * @ClassName: Connection
 * @Description: 定义连接
 * @Author: LIULJ
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public interface Connection<T> {

    /**
     * 新建连接状态
     */
    byte STATUS_NEW = 0;

    /**
     * 已连接状态
     */
    byte STATUS_CONNECTED = 1;

    /**
     * 断开连接状态
     */
    byte STATUS_DISCONNECTED = 2;

    /**
     * 初始化
     *
     * @param clientChannel
     */
    void init(SocketChannel clientChannel);

    /**
     * 获取上下文
     *
     * @return
     */
    SessionContext getSessionContext();

    /**
     * 设置上下文
     *
     * @param context
     */
    void setSessionContext(SessionContext context);

    /**
     * 发送Packet
     *
     * @param t
     * @return
     */
    ChannelFuture channelSend(T t);

    /**
     * 发送Packet,并监听发送状态
     *
     * @param t
     * @param listener
     * @return
     */
    ChannelFuture channelSend(T t, ChannelFutureListener listener);

    /**
     * 关闭连接
     *
     * @return
     */
    ChannelFuture channelClose();

    /**
     * 向SocketIO客户端发送事件
     *
     * @param eventName
     * @param data
     */
    void sendEvent(String eventName, Object... data);

    /**
     * SocketIO断开连接
     */
    void disConnect();

    /**
     * 获取链接ID
     *
     * @return
     */
    String getId();

    /**
     * 获取连接的用户ID
     * @return
     */
    String getUserId();

    /**
     * 获取设备ID
     * @return
     */
    String getDeviceId();

    /**
     * 设置设备ID
     * @param deviceId
     * @return
     */
    void setDeviceId(String deviceId);

    /**
     * 设置当前用户ID，一般在握手之后设置
     * @param userId
     */
    void setUserId(String userId);

    /**
     * 是否连接
     *
     * @return
     */
    boolean isConnected();

    /**
     * 是否读超时
     *
     * @return
     */
    boolean isReadTimeout();

    /**
     * 是否写超时
     *
     * @return
     */
    boolean isWriteTimeout();

    /**
     * 更新最后读时间
     */
    void updateLastReadTime();

    /**
     * 更新最后写时间
     */
    void updateLastWriteTime();

    /**
     * 获取通道
     *
     * @return
     */
    SocketChannel getSocketClient();
}
