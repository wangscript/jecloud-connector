/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.exception.handler;

import com.je.connector.base.connection.Connection;
import com.je.connector.base.message.MessageHandler;
import com.je.connector.base.message.model.BaseMessage;
import com.je.connector.base.protocol.Packet;

/**
 * 消息处理抽象类
 *
 * @ProjectName: instant-message
 * @Package: com.connector.base.handler
 * @ClassName: BaseMessageHandler
 * @Description: 定义消息处理抽象类
 * @Author: LIULJ
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public abstract class BaseMessageHandler<T extends BaseMessage> implements MessageHandler {

    /**
     * 解码
     *
     * @param packet
     * @param connection
     * @return
     */
    public abstract T decode(Packet packet, Connection connection);

    /**
     * 封装统一处理
     *
     * @param message
     */
    public abstract void handle(T message);

    @Override
    public void handle(Packet packet, Connection connection) {
        T t = decode(packet, connection);
        if (t != null) {
            t.decodeBody();
            handle(t);
        }
    }
}
