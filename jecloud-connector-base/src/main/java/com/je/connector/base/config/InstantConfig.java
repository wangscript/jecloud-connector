/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.config;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * 及时通讯配置项定义
 *
 * @ProjectName: instant-message
 * @Package: com.connector.base.InstantConfig
 * @ClassName: InstantConfig
 * @Description: 描述推送服务配置
 * @Author: LIULJ
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public interface InstantConfig {

    Config cfg = load();

    static Config load() {
        //扫描加载所有可用的配置文件
        Config config = ConfigFactory.load("instant.conf");
        return config;
    }

    /**
     * 加载制定资源文件
     * @param resource
     * @return
     */
    static Config load(String resource) {
        //扫描加载所有可用的配置文件
        Config config = ConfigFactory.load(resource);
        return config;
    }

    interface server {
        Config cfg = InstantConfig.cfg.getObject("server").toConfig();
        /**
         * 服务主机
         */
        String host = cfg.getString("host");
        /**
         * 消息是否压缩
         */
        boolean compress = cfg.getBoolean("compress");
        /**
         * 是否加密
         */
        boolean encryption = cfg.getBoolean("encryption");
        /**
         * 会话超时时间
         */
        int sessionExpiredTime = cfg.getInt("sessionExpiredTime");

        /**
         * 原生WebSocket服务定义
         */
        interface websocket {
            Config cfg = server.cfg.getObject("websocket").toConfig();
            int port = cfg.getInt("port");
            String wsPath = cfg.getString("wsPath");
        }

        /**
         * 消息定义
         */
        interface jmx {
            Config cfg = server.cfg.getObject("jmx").toConfig();
            String topic = cfg.getString("topic");
        }
    }
}
