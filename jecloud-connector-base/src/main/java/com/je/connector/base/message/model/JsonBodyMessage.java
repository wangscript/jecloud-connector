/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.message.model;

import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.je.connector.base.connection.Connection;
import com.je.connector.base.protocol.Packet;

/**
 * Json消息体格式
 *
 * @ProjectName: instant-message
 * @Package: com.connector.base.message.model
 * @ClassName: JsonBodyMessage
 * @Description: 定义Json消息体格式
 * @Author: LIULJ
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public abstract class JsonBodyMessage extends BaseMessage {

    public JsonBodyMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    public JsonBodyMessage() {

    }

    @Override
    public byte[] encode() {
        JSONObject resultObj = fieldToJson();
        return resultObj == null ? new byte[]{} : resultObj.toJSONString().getBytes(Charsets.UTF_8);
    }

    @Override
    public void decode(byte[] body) {
        String json = new String(body, Charsets.UTF_8);
        if (!Strings.isNullOrEmpty(json)) {
            JSONObject jsonBody = JSONObject.parseObject(json);
            jsonToFields(jsonBody);
        }
    }

    /**
     * 转成消息下字段信息
     *
     * @param jsonBody
     */
    public abstract void jsonToFields(JSONObject jsonBody);

    /**
     * 根据消息下字段信息格式化为Json
     *
     * @return
     */
    public abstract JSONObject fieldToJson();
}
