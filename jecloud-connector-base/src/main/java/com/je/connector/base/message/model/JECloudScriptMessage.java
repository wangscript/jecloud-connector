/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.message.model;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Charsets;
import com.je.connector.base.connection.Connection;
import com.je.connector.base.protocol.Command;
import com.je.connector.base.protocol.Packet;

import java.util.HashMap;
import java.util.Map;

/**
 * 脚本消息
 */
public class JECloudScriptMessage extends JECloudMessage<String> {

    /**
     * 执行的脚本
     */
    private String script;
    /**
     * 脚本参数
     */
    private Map<String,Object> params;

    public JECloudScriptMessage(Packet packet, Connection connection) {
        super(packet, connection);
        this.type = MessageType.SCRIPT_MESSAGE_TYPE;
    }

    public JECloudScriptMessage(String busKey, String content) {
        this.type = MessageType.SCRIPT_MESSAGE_TYPE;
        this.busKey = busKey;
        this.content = content;
    }

    public JECloudScriptMessage(String busKey, String content,String script,Map<String,Object> params) {
        this.type = MessageType.SCRIPT_MESSAGE_TYPE;
        this.busKey = busKey;
        this.content = content;
        this.script = script;
        this.params = params;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    @Override
    public void jsonToFields(JSONObject jsonBody) {
        this.setType(jsonBody.getString("type"));
        this.setBusKey(jsonBody.getString("busType"));
        this.setContent(jsonBody.getString("content"));
        this.setScript(jsonBody.getString("script"));
        Map<String,Object> paramsMap = new HashMap<>();
        JSONObject params = jsonBody.getJSONObject("params");
        if(params != null && !params.isEmpty()){
            for (String eachKey : params.keySet()) {
                paramsMap.put(eachKey,params.get(eachKey));
            }
        }
    }

    @Override
    public JSONObject fieldToJson() {
        JSONObject messageObj = new JSONObject();
        messageObj.put("type",getType());
        messageObj.put("busType",getBusKey());
        messageObj.put("content",getContent());
        messageObj.put("script",getScript());
        messageObj.put("params",getParams() == null?null: JSON.toJSONString(getParams()));
        return messageObj;
    }

    public static Packet buildPacket(JECloudMessage msg) {
        JSONObject messageObj = msg.fieldToJson();
        String message = messageObj.toJSONString();

        Packet packet = new Packet();
        packet.setCmd(Command.PUSH.cmd);
        packet.setBodyLength(message.length());
        packet.setType((byte) 1);
        packet.setBody(message.getBytes(Charsets.UTF_8));
        return packet;
    }

}
