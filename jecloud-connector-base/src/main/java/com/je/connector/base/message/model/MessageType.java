/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.message.model;

/**
 * 业务消息类型
 */
public class MessageType {

    /**
     * 握手消息类型
     */
    public static final String HANDSHAKE_TYPE = "0";
    /**
     * jeplus 推送消息类型
     */
    public static final String SYS_MESSAGE_TYPE = "1";
    /**
     * jeplus 脚本消息类型
     */
    public static final String SCRIPT_MESSAGE_TYPE = "2";
    /**
     * 握手未读消息推送消息类型
     */
    public static final String NOREAD_MESSAGE_TYPE = "3";
    /**
     * 通知消息类型
     */
    public static final String NOTICE_MESSAGE_TYPE = "4";

}
