/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.protocol;

/**
 * 即时通讯命令类型
 *
 * @ProjectName: instant-message
 * @Package: com.connector.base.protocol
 * @ClassName: Command
 * @Description: 定义即时通讯命令类型
 * @Author: LIULJ
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public enum Command {

    /**
     * 未知命令
     */
    UNKNOWN(0),
    /**
     * 心跳
     * type:ping->pong
     */
    HEARTBEAT(1),
    /**
     * 握手
     */
    HANDSHAKE(2),
    /**
     * 踢出
     */
    KICK(3),
    /**
     * 推送消息
     */
    PUSH(4),
    /**
     * 成功消息
     */
    OK(5),
    /**
     * 错误消息
     */
    ERROR(6),
    /**
     * 握手检测---证书检查
     */
    HANDSHAKECHECK(7);

    Command(int cmd) {
        this.cmd = (byte) cmd;
    }

    public final byte cmd;

}
