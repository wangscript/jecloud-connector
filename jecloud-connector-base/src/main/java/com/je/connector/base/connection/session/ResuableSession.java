/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.connection.session;

import com.je.connector.base.connection.SessionContext;
import lombok.Getter;
import lombok.Setter;

/**
 * 可恢复Session定义
 *
 * @ProjectName: instant-message
 * @Package: com.connector.base.connection
 * @ClassName: ResuableSession
 * @Description: 可恢复Session定义
 * @Author: LIULJ
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Getter
@Setter
public final class ResuableSession {

    private String sessionId;
    private long expireTime;
    private SessionContext context;

    public static String encode(SessionContext context) {
        StringBuffer sb = new StringBuffer();
        sb.append(context.getOsName()).append(',');
        sb.append(context.getOsVersion()).append(',');
        sb.append(context.getClientVersion()).append(',');
        sb.append(context.getDeviceId()).append(',');
        return sb.toString();
    }

    public static ResuableSession decode(String value) {
        String[] array = value.split(",");
        if (array.length != 4) {
            return null;
        }
        SessionContext context = new SessionContext();
        context.setOsName(array[0]);
        context.setOsVersion(array[1]);
        context.setClientVersion(array[2]);
        context.setDeviceId(array[3]);
        ResuableSession session = new ResuableSession();
        session.context = context;
        return session;
    }

}
