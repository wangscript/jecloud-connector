/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.connection.registry;

import com.je.connector.base.connection.Connection;
import com.je.connector.base.connection.channel.SocketChannel;

import javax.validation.executable.ValidateOnExecution;
import java.util.List;

/**
 * 定义连接注册器
 *
 * @ProjectName: instant-message
 * @Package: com.connector.base.connection
 * @ClassName: ConnectionRegistry
 * @Description: 定义连接注册器
 * @Author: LIULJ
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public interface ConnectionRegistry {

    /**
     * 已创建，但未初始化
     */
    int STATUS_CREATEED = 0;
    /**
     * 已初始化
     */
    int STATUS_INITED = 1;
    /**
     * 已销毁
     */
    int STATUS_DESTROY = 2;

    /**
     * 本地是否存在
     * @param connection
     * @return
     */
    boolean localExist(Connection connection);

    /**
     * 校验是否存在此链接
     * @return
     */
    boolean exist(Connection connection);

    /**
     * 获取链接
     *
     * @param clientChannel
     * @return
     */
    Connection get(SocketChannel clientChannel);

    /**
     * 移除关闭链接
     *
     * @param clientChannel
     * @return
     */
    Connection removeAndClose(SocketChannel clientChannel);

    /**
     * 增加连接
     *
     * @param connection
     */
    void add(Connection connection);

    /**
     * 设置连接用户
     * @param connection
     */
    void putUser(Connection connection);

    /**
     * 获取当前链接数量
     *
     * @return
     */
    long getConnectionNum();

    /**
     * 获取所有连接
     * @return
     */
    List<Connection> getConnections();

    /**
     * 获取本机所有连接
     * @return
     */
    List<Connection> getLocalConnections();

    /**
     * 注册器初始化
     */
    void init();

    /**
     * 注册器销毁
     */
    void destroy();
}
