/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.connector.base.message.model;

import com.alibaba.fastjson2.JSONObject;
import com.je.connector.base.connection.Connection;
import com.je.connector.base.protocol.Command;
import com.je.connector.base.protocol.Packet;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

/**
 * 握手消息
 *
 * @ProjectName: instant-message
 * @Package: com.connector.base.message.model
 * @ClassName: HandshakeMessage
 * @Description: 定义TCP握手消息定义
 * @Author: LIULJ
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public final class HandshakeMessage extends JsonBodyMessage {

    private String clientId;
    private String userId;
    private String type;
    private String contentType;
    private String receiver;
    private String body;
    private String deviceId;
    private String osName;
    private String osVersion;
    private String clientVersion;
    //是不是移动设备1:不是,2是
    private String mobile;
    private long timestamp;
    public int minHeartbeat;
    public int maxHeartbeat;
    public String appid;
    public String cid;


    public HandshakeMessage(Connection connection) {
        super(new Packet(Command.HANDSHAKE.cmd, Packet.NORMAL_REQ_TYPE, genSessionId()), connection);
    }

    public HandshakeMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void jsonToFields(JSONObject jsonBody) {
        clientId = jsonBody.containsKey("clientId") ? jsonBody.getString("clientId") : null;
        userId = jsonBody.containsKey("userId") ? jsonBody.getString("userId") : null;
        type = jsonBody.containsKey("type") ? jsonBody.getString("type") : null;
        contentType = jsonBody.containsKey("contentType") ? jsonBody.getString("contentType") : null;
        receiver = jsonBody.containsKey("receiver") ? jsonBody.getString("receiver") : null;
        body = jsonBody.containsKey("body") ? jsonBody.getString("body") : null;
        deviceId = jsonBody.containsKey("deviceId") ? jsonBody.getString("deviceId") : null;
        osName = jsonBody.containsKey("osName") ? jsonBody.getString("osName") : null;
        osVersion = jsonBody.containsKey("osVersion") ? jsonBody.getString("osVersion") : null;
        clientVersion = jsonBody.containsKey("clientVersion") ? jsonBody.getString("clientVersion") : null;
        mobile = jsonBody.containsKey("mobile") ? jsonBody.getString("mobile") : "1";
        timestamp = System.currentTimeMillis();
        minHeartbeat = jsonBody.containsKey("minHeartbeat") ? jsonBody.getInteger("minHeartbeat") : 0;
        maxHeartbeat = jsonBody.containsKey("maxHeartbeat") ? jsonBody.getInteger("maxHeartbeat") : 0;
        appid = jsonBody.containsKey("appId") ? jsonBody.getString("appId") : null;
        cid = jsonBody.containsKey("cid") ? jsonBody.getString("cid") : null;
    }

    @Override
    public JSONObject fieldToJson() {
        JSONObject resultObj = new JSONObject();
        resultObj.put("clientId",clientId);
        resultObj.put("userId", userId);
        resultObj.put("type", type);
        resultObj.put("contentType", contentType);
        resultObj.put("receiver", receiver);
        resultObj.put("body", body);
        resultObj.put("appid", appid);
        resultObj.put("cid", cid);
        resultObj.put("deviceId", deviceId);
        resultObj.put("osName", osName);
        resultObj.put("osVersion", osVersion);
        resultObj.put("clientVersion", clientVersion);
        resultObj.put("mobile", mobile);
        resultObj.put("timestamp", timestamp);
        resultObj.put("minHeartbeat", minHeartbeat);
        resultObj.put("maxHeartbeat", maxHeartbeat);
        return resultObj;
    }

    public static HandshakeMessage buildResponse(BaseMessage src) {
        HandshakeMessage response = null;
        if (Packet.NORMAL_REQ_TYPE == src.packet.getType()) {
            response = new HandshakeMessage(new Packet(Command.HANDSHAKE.cmd, Packet.NORMAL_RESP_TYPE, src.packet.getSessionId()), src.connection);
        } else if (Packet.CLUSTER_REQ_TYPE == src.packet.getType()) {
            response = new HandshakeMessage(new Packet(Command.HANDSHAKE.cmd, Packet.CLUSTER_RESP_TYPE, src.packet.getSessionId()), src.connection);
        }
        return response;
    }

    protected static int genSessionId() {
        return ID_SEQ.incrementAndGet();
    }
}
